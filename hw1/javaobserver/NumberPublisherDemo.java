package javaobserver;

public class NumberPublisherDemo {

	public static void main(String[] args) throws InterruptedException {

		ConcreteSubject subject = new ConcreteSubject();

		new BinaryView(subject);
		new DecimalView(subject);
		new HexaDecimalView(subject);
		
		int publishCount = 5;
		
		for (int i =0; i<publishCount; i++){
			int number = i*20;
			System.out.println("\nPublishing with JavaUtils : " + number);
			subject.setState(number);

			Thread.sleep(1000);
		}

	}

}
