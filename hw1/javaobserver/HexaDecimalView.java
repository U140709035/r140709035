package javaobserver;

import java.util.Observable;
import java.util.Observer;

public class HexaDecimalView implements Observer {

	public HexaDecimalView(Observable observable){
		observable.addObserver(this);
	}

	public void update(Observable observable, Object arg) {
		if (observable instanceof ConcreteSubject) {
			ConcreteSubject subject = (ConcreteSubject) observable;

			System.out.println( "Hex Number: " + Integer.toHexString( subject.getState() ).toUpperCase() );


		}
	}




}
