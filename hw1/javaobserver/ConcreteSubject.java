package javaobserver;
import java.util.Observable;

public class ConcreteSubject extends Observable {
	private int state;

	public void setState(int state) {
        this.state = state;
        setChanged();
        notifyObservers();
    }

    public int getState() {
        return state;
    }



}
