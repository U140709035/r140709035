package javaobserver;

import java.util.Observable;
import java.util.Observer;

public class BinaryView implements Observer {
	public BinaryView(Observable observable){
		observable.addObserver(this);
	}
	public void update(Observable observable, Object arg) {
		if (observable instanceof ConcreteSubject) {
			ConcreteSubject subject = (ConcreteSubject) observable;

			System.out.println( "Binary Number: " + Integer.toBinaryString( subject.getState() ) );

		}
	}






}
