package myobserver;

public interface Observer{
	public void update(int number);
}
