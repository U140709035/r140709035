package myobserver;

public class DecimalView implements Observer {
	private String decimalNumber;
	NumberPublisher publisher;
	
	public DecimalView(NumberPublisher publisher) {
		this.publisher = publisher;
		publisher.AddObserver(this);

	}
	
	public void update(int number) {
		this.decimalNumber = Integer.toString(number);
		System.out.println(decimalNumber);

	}
	
	
		
	
	}

