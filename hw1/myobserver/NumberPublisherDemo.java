package myobserver;

public class NumberPublisherDemo {

	public static void main(String[] args) throws InterruptedException {
		NumberPublisher publisher = new NumberPublisher();
		
		BinaryView binaryNumber = new BinaryView(publisher);
		HexaDecimalView hexNumber = new HexaDecimalView(publisher);
		DecimalView DecimalNumber = new DecimalView(publisher);
		
		int publishCount = 5;
		
		for (int i =0; i<publishCount; i++){
			int number = i*20;
			System.out.println("\nPublishing:" + number);
			publisher.SetNumber(number);
			Thread.sleep(1000);
		}
		

	}

}
