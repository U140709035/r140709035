package myobserver;

public class BinaryView implements Observer {
	private String binaryNumber;
	NumberPublisher publisher;
	
	public BinaryView(NumberPublisher publisher) {
		this.publisher = publisher;
		publisher.AddObserver(this);
	}
	
	public void update(int number) {
		this.binaryNumber = Integer.toBinaryString(number);
		System.out.println(binaryNumber); 
	}			
	}
	
