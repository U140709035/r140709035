package myobserver;

public class HexaDecimalView implements Observer {
	private String hexaDecimalNumber;
	NumberPublisher publisher;

	public HexaDecimalView(NumberPublisher publisher) {
		this.publisher = publisher;
		publisher.AddObserver(this);

	}
	
	public void update(int number) {
		this.hexaDecimalNumber = Integer.toHexString(number);
		System.out.println(hexaDecimalNumber);
		
	
	}
}
