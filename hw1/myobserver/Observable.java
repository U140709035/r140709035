package myobserver;

public interface Observable {
	public void AddObserver(Observer o);
	public void RemoveObserver(Observer o);
	public void Notify();
}
