package myobserver;

import java.util.*;


public class NumberPublisher implements Observable {
	private ArrayList<Observer> observerlist;
	private int number;
	
	public NumberPublisher() {
		observerlist = new ArrayList<Observer>();

	}
	public void AddObserver(Observer o) {
		this.observerlist.add(o);
	}
	public void RemoveObserver(Observer o) {
		int i = observerlist.indexOf(o);
		if (i >= 0) {
			observerlist.remove(i);
		}
	}

	public void Notify() {
		for(Observer observer : observerlist) {
			observer.update(number);
		}
		
	}
	
	public int GetNumber() {
		return this.number;
	}
	
	public void SetNumber(int number) {
		this.number = number;
		Notify();
	}

}
